// https://github.com/shelljs/shelljs
"use strict"
var electronInstaller = require("electron-winstaller")

function win() {
  var resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: "/home/ash/opol2/",
    outputDirectory: "/home/ash/opol2/installer64",
    authors: "My App Inc.",
    exe: "myapp.exe",
  })

  resultPromise
    .then(
      () => console.log("It worked!"),
      e => console.log(`No dice: ${e.message}`),
    )
    .then(() => {
      return
    })
}
require("./check-versions")()
require("shelljs/global")

env.NODE_ENV = "production"

const ora = require("ora")
const path = require("path")
const chalk = require("chalk")
const webpack = require("webpack")
const config = require("../config")
const webpackConfig = require("./webpack.prod.conf")

const spinner = ora("building for production...")
spinner.start()

const assetsPath = path.join(
  config.build.assetsRoot,
  config.build.assetsSubDirectory,
)
rm("-rf", assetsPath)
mkdir("-p", assetsPath)
cp("-R", "assets/*", assetsPath)

const compiler = webpack(webpackConfig)
const ProgressPlugin = require("webpack/lib/ProgressPlugin")
compiler.apply(new ProgressPlugin())

compiler.run((err, stats) => {
  spinner.stop()
  if (err) throw err
  process.stdout.write(
    stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false,
    }) + "\n\n",
  )

  console.log(chalk.cyan("  Build complete.\n"))
  console.log(
    chalk.yellow(
      "  Tip: built files are meant to be served over an HTTP server.\n" +
        "  Opening index.html over file:// won't work.\n",
    ),
  )
})
