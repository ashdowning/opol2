// export default [
//   {
//     name: "id",
//     sortField: "id",
//     visible: false,
//   },

//   {
//     title: "Level",
//     name: "level",
//     sortField: "serial",
//   },
//   {
//     Title: "message",
//     name: "message",
//     sortField: "message",
//   },
// ]
// {
//   title: "Model",
//   name: "model.name",
//   sortField: "model.name",
// },
// {
//   title: "Category",
//   name: "category.name",
//   sortField: "category.name",
// },
// {
//   title: "Status",
//   name: "status_label.name",
//   sortField: "status_label.name",
// },
// {
//   title: "Assigned To",
//   name: "assigned_to.name",
//   sortField: "assigned_to.name",
// },
// {
//   title: "Location",
//   name: "rtd_location",
//   sortField: "rtd_location",
//   callback: "deployedLocationFormatter",
// },
// {
//   name: "__slot:actions",
//   title: "Slot Actions",
//   titleClass: "center aligned",
//   dataClass: "center aligned",
// },
// {
//   name: "__slot:actions2",
//   title: "Slot Actions",
//   titleClass: "center aligned",
//   dataClass: "center aligned",
// },
//{
/* <th data-field="icon" style="width: 40px;" class="hidden-xs" data-formatter="iconFormatter"></th>
<th class="col-sm-3" data-searchable="false" data-sortable="true" data-field="created_at" data-formatter="dateDisplayFormatter">Date</th>
<th class="col-sm-2" data-field="admin" data-formatter="usersLinkObjFormatter">Admin</th>
<th class="col-sm-2" data-field="action_type">Action</th>
<th class="col-sm-1" data-field="type" data-formatter="itemTypeFormatter">Type</th>
<th class="col-sm-3" data-field="item" data-formatter="polymorphicItemFormatter">Item</th>
<th class="col-sm-2" data-field="target" data-formatter="polymorphicItemFormatter">To</th>
<th class="col-sm-1" data-field="note">Notes</th>
<th class="col-sm-2" data-field="log_meta" data-visible="false" data-formatter="changeLogFormatter">Changed</th> */
//}
export default [
  {
    name: "id",
    sortField: "id",
    visible: false,
  },
  {
    name: "icon",
    sortField: "icon",
    visible: false,
    callback: "iconFormatter",
  },
  {
    name: "created_at",
    title: "Date",
    sortField: "created_at",
    callback: "dateDisplayFormatter",
  },
  {
    name: "admin",
    title: "Admin",
    sortField: "admin",
    callback: "polymorphicItemFormatter",
  },
  {
    name: "log_meta",
    title: "Note",
    visible: false,
    sortField: "note",
    callback: "changeLogFormatter",
  },

  {
    name: "item",
    title: "Item",
    sortField: "item",
    callback: "polymorphicItemFormatter",
  },
  {
    name: "action_type",
    title: "Action",
    sortField: "action_type",
  },
  {
    name: "type",
    title: "Type",
    sortField: "type",
    callback: "itemTypeFormatter",
  },
  {
    name: "target",
    title: "To",
    sortField: "target",
    callback: "polymorphicItemFormatter",
  },
  {
    name: "note",
    title: "Note",
    sortField: "note",
  },
]
