export default [
  {
    name: "id",
    sortField: "id",
    visible: false,
  },
  // {
  //   name: "__checkbox",
  //   titleClass: "text-center",
  //   dataClass: "text-center",
  // },
  {
    name: "image",
    sortField: "image",
    titleClass: "text-center",
    callback: "imageFormatter",
  },
  {
    name: "asset_tag",
    sortField: "asset_tag",
    // callback: 'linkFormatter'
  },

  {
    title: "Serial",
    name: "serial",
    sortField: "serial",
  },
  {
    title: "Model",
    name: "model.name",
    sortField: "model.name",
  },
  {
    title: "Category",
    name: "category.name",
    sortField: "category.name",
  },
  {
    title: "Status",
    name: "status_label.name",
    sortField: "status_label.name",
  },
  {
    title: "Assigned To",
    name: "assigned_to.name",
    sortField: "assigned_to.name",
  },
  {
    title: "Location",
    name: "rtd_location",
    sortField: "rtd_location",
    callback: "deployedLocationFormatter",
  },
  {
    name: "__slot:actions",
    title: "Slot Actions",
    titleClass: "center aligned",
    dataClass: "center aligned",
  },
  {
    name: "__slot:actions2",
    title: "Slot Actions",
    titleClass: "center aligned",
    dataClass: "center aligned",
  },
  // {
  //   name: "__component:custom-actions",
  //   title: "Actions",
  //   titleClass: "center aligned",
  //   dataClass: "center aligned",
  // },
]
