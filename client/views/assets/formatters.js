// var formatters = [
//     'hardware',
//     'accessories',
//     'consumables',
//     'components',
//     'locations',
//     'users',
//     'manufacturers',
//     'statuslabels',
//     'models',
//     'licenses',
//     'categories',
//     'suppliers',
//     'departments',
//     'companies',
//     'depreciations',
//     'fieldsets',
//     'groups'
// ];

// for (var i in formatters) {
//     window[formatters[i] + 'LinkFormatter'] = genericRowLinkFormatter(formatters[i]);
//     window[formatters[i] + 'LinkObjFormatter'] = genericColumnObjLinkFormatter(formatters[i]);
//     window[formatters[i] + 'ActionsFormatter'] = genericActionsFormatter(formatters[i]);
//     window[formatters[i] + 'InOutFormatter'] = genericCheckinCheckoutFormatter(formatters[i]);
// }

export default [
  // This is  gross, but necessary so that we can package the API response
  // for custom fields in a more useful way.
  function customFieldsFormatter(value, row) {
    if (!this || !this.title) {
      return ""
    }

    var field_column = this.title

    // Pull out any HTMl that might be passed via the presenter
    // (for example, the locked icon for encrypted fields)
    var field_column_plain = field_column.replace(/<(?:.|\n)*?> ?/gm, "")
    if (row.custom_fields && row.custom_fields[field_column_plain]) {
      // If the field type needs special formatting, do that here
      if (
        row.custom_fields[field_column_plain].field_format &&
        row.custom_fields[field_column_plain].value
      ) {
        if (row.custom_fields[field_column_plain].field_format == "URL") {
          return (
            '<a href="' +
            row.custom_fields[field_column_plain].value +
            '" target="_blank" rel="noopener">' +
            row.custom_fields[field_column_plain].value +
            "</a>"
          )
        } else if (
          row.custom_fields[field_column_plain].field_format == "EMAIL"
        ) {
          return (
            '<a href="mailto:' +
            row.custom_fields[field_column_plain].value +
            '">' +
            row.custom_fields[field_column_plain].value +
            "</a>"
          )
        }
      }
      return row.custom_fields[field_column_plain].value
    }
  },

  function createdAtFormatter(value) {
    if (value && value.date) {
      return value.date
    }
  },

  function groupsFormatter(value) {
    if (value) {
      var groups = ""
      for (var index in value.rows) {
        groups +=
          '<a href="http://168.235.96.46:8080/admin/groups/' +
          value.rows[index].id +
          '" class="label label-default"> ' +
          value.rows[index].name +
          "</a> "
      }
      return groups
    }
  },

  function changeLogFormatter(value) {
    var result = ""
    for (var index in value) {
      result +=
        index +
        ": <del>" +
        value[index].old +
        '</del>  <i class="fa fa-long-arrow-right" aria-hidden="true"></i> ' +
        value[index].new +
        "<br>"
    }

    return result
  },

  function deployedLocationFormatter(row, value) {
    if (row && row != undefined) {
      return (
        '<a href="http://168.235.96.46:8080/locations/' +
        row.id +
        '"> ' +
        row.name +
        "</a>"
      )
    } else if (value.rtd_location) {
      return (
        '<a href="http://168.235.96.46:8080/locations/' +
        value.rtd_location.id +
        '" data-tooltip="true" title="Default Location"> ' +
        value.rtd_location.name +
        "</a>"
      )
    }
  },

  function groupsAdminLinkFormatter(value, row) {
    return (
      '<a href="http://168.235.96.46:8080/admin/groups/' +
      row.id +
      '"> ' +
      value +
      "</a>"
    )
  },

  function trueFalseFormatter(value, row) {
    if (value && (value == "true" || value == "1")) {
      return '<i class="fa fa-check text-success"></i>'
    } else {
      return '<i class="fa fa-times text-danger"></i>'
    }
  },

  function dateDisplayFormatter(value, row) {
    if (value) {
      return value.formatted
    }
  },

  function iconFormatter(value, row) {
    if (value) {
      return '<i class="' + value + '"></i>'
    }
  },

  function emailFormatter(value, row) {
    if (value) {
      return '<a href="mailto:' + value + '"> ' + value + "</a>"
    }
  },

  function linkFormatter(value, row) {
    if (value) {
      return '<a href="' + value + '"> ' + value + "</a>"
    }
  },

  function assetCompanyFilterFormatter(value, row) {
    if (value) {
      return (
        '<a href="http://168.235.96.46:8080/hardware/?company_id=' +
        row.id +
        '"> ' +
        value +
        "</a>"
      )
    }
  },

  function assetCompanyObjFilterFormatter(value, row) {
    if (row && row.company) {
      return (
        '<a href="http://168.235.96.46:8080/hardware/?company_id=' +
        row.company.id +
        '"> ' +
        row.company.name +
        "</a>"
      )
    }
  },

  function usersCompanyObjFilterFormatter(value, row) {
    if (value) {
      return (
        '<a href="http://168.235.96.46:8080/users/?company_id=' +
        row.id +
        '"> ' +
        value +
        "</a>"
      )
    } else {
      return value
    }
  },

  function employeeNumFormatter(value, row) {
    if (row && row.assigned_to && row.assigned_to.employee_number) {
      return (
        '<a href="http://168.235.96.46:8080/users/' +
        row.assigned_to.id +
        '"> ' +
        row.assigned_to.employee_number +
        "</a>"
      )
    }
  },

  function orderNumberObjFilterFormatter(value, row) {
    if (value) {
      return (
        '<a href="http://168.235.96.46:8080/hardware/?order_number=' +
        row.order_number +
        '"> ' +
        row.order_number +
        "</a>"
      )
    }
  },

  function imageFormatter(value, row) {
    if (value) {
      return (
        '<a href="' +
        value +
        '" data-toggle="lightbox" data-type="image"><img src="' +
        value +
        '" style="max-height: 50px; width: auto;" class="img-responsive"></a>'
      )
    }
  },

  function sumFormatter(data) {
    var field = this.field
    var total_sum = data.reduce(function(sum, row) {
      return sum + (parseFloat(row[field]) || 0)
    }, 0)
    return total_sum.toFixed(2)
  },
]
