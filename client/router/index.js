import Vue from "vue"
import Router from "vue-router"
import menuModule from "vuex-store/modules/menu"
Vue.use(Router)
console.log()
export default new Router({
  mode: "hash", // Demo is living in GitHub.io, so required!
  linkActiveClass: "is-active",
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      name: "Home",
      path: "/",
      component: require("../views/Home"),
    },
    {
      name: "User",
      path: "/user",
      component: require("../views/user/"),
    },
    {
      name: "Login",
      path: "/login",
      component: require("../views/auth/Login"),
    },
    {
      name: "Login Firebase",
      path: "/login-firebase",
      component: require("../views/auth/LoginFirebase"),
    },
    {
      name: "Live View",
      path: "/liveview/",
      meta: {
        icon: "fa-tachometer",
        link: "liveview/index.vue",
      },
      component: require("../views/liveview/index.vue"),
    },
    {
      name: "Your Dashboard",
      path: "/",
      meta: {
        icon: "fa-tachometer",
        link: "dashboard/index.vue",
      },
      component: require("../views/Home"),
    },
    {
      name: "Hardware",
      path: "/hardware",
      meta: {
        icon: "fa-tachometer",
        link: "assets/Hardware.vue",
      },
      component: require("../views/assets/Hardware"),
    },
    {
      name: "HardwareDetail",
      path: "/hardwaredetail/:id",
      meta: {
        icon: "fa-tachometer",
        link: "assets/HardwareDetail.vue",
      },
      component: require("../views/assets/hardware/HardwareDetail"),
    },
    {
      name: "HardwareEdit",
      path: "/hardwareedit/:id",
      meta: {
        icon: "fa-tachometer",
        link: "assets/HardwareEdit.vue",
      },
      component: require("../views/assets/hardware/HardwareEdit"),
    },
    {
      name: "Consumables",
      path: "/consumables",
      meta: {
        icon: "fa-tachometer",
        link: "assets/Consumables.vue",
      },
      component: require("../views/assets/Consumables"),
    },
    {
      name: "Groups",
      path: "/",
      meta: {
        icon: "fa-tachometer",
        link: "assets/Groups.vue",
      },
      component: require("../views/Home"),
    },
    {
      name: "Profile",
      path: "/profile",
      meta: {
        icon: "fa-tachometer",
        link: "employees/Profile.vue",
      },
      component: require("../views/user/"),
    },
    {
      name: "Applicants",
      path: "/",
      meta: {
        icon: "fa-tachometer",
        link: "employees/Applicants.vue",
      },
      component: require("../views/Home"),
    },

    {
      name: "Documents",
      path: "/",
      meta: {
        icon: "fa-tachometer",
        link: "documents/index.vue",
      },
      component: require("../views/Home"),
    },
    {
      name: "Logs",
      path: "/logs",
      meta: {
        icon: "fa-tachometer",
        link: "logs/index.vue",
      },
      component: require("../views/logs/"),
    },
    // ...generateRoutesFromMenu(menuModule.state.items),
    {
      path: "*",
      redirect: "/",
    },
  ],
})

// Menu should have 2 levels.
// function generateRoutesFromMenu (menu = [], routes = []) {
//   for (let i = 0, l = menu.length; i < l; i++) {
//     let item = menu[i]
//     if (item.path) {
//       routes.push(item)
//     }
//     if (!item.component) {
//       generateRoutesFromMenu(item.children, routes)
//     }
//   }
//   return routes
// }
