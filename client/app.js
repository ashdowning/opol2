import Vue from "vue"
import * as firebase from "firebase"
import config from "./firebase-setup/config"
import axios from "axios"
import VueAxios from "vue-axios"
import NProgress from "vue-nprogress"
import { sync } from "vuex-router-sync"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import * as filters from "./filters"
import { TOGGLE_SIDEBAR } from "vuex-store/mutation-types"
// css files
// import "va/lib/css"

// js files
// import "va/lib/script"
// import VeeValidate from "vee-validate"
// import veeValidateConfig from "./utils"
// adding firebase to the Vue.prototype allows usage of firebase in your vue components by using this.$firebase

// var ramnodeToken =
//   "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjY4Zjg1MzAzNDlhMDhlNzAwODYyYzcwNDM5OTRjMDhhNTBlMWI2NGE0ZWYxNjA0NjM3ZjcwNjc4Y2QyYjViMmQyM2NmNTU0NzExYmMwNTg5In0.eyJhdWQiOiIxIiwianRpIjoiNjhmODUzMDM0OWEwOGU3MDA4NjJjNzA0Mzk5NGMwOGE1MGUxYjY0YTRlZjE2MDQ2MzdmNzA2NzhjZDJiNWIyZDIzY2Y1NTQ3MTFiYzA1ODkiLCJpYXQiOjE1MTQ1MzE4OTYsIm5iZiI6MTUxNDUzMTg5NiwiZXhwIjoxNTQ2MDY3ODk2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.dyzpV0D-KkEcmPwTXXnCYkSbvKlqCBYDZMBgzNQvRz9ydgygOVEX8RcRigEjtAIYD4vquok1aq3SmHm_4ZNuOyux82sZt6HiucBJCWzZhWJ1t5sEeMuQaWB2hDxQEu8HbS-xRYl4df94BP_vJM-v-FfKCJ3x-QZaY6Srmu9m8hzZWGfG-U7jbBDxDV1RFMyD6506i0B1Z-cTrBBSY4q0V4Ke6JF7LYzX_sm3KoV6NOEjGqKtNqWwOOfCXfdwgsBV3GUCZCRDBdBXQSACBN8cUl5syalP9tIIuZ4Zzzcxff-4WzuEWwkQL5gYSX5bESG59NmEDFyGI7dmZXt9ytHOsQZGy0bOgRJkNtqAID1F-0TXduMr0pJzszRtVawqqrVWQ2oS2UvI8JKzv4F5c7m9A6l3G9i3CnWT5wgN8QLNsh0D7PlrmRQQl5P8D4WjxfHsvyKn7cp4oH605Rupbx6UG-TsFR4RoNQQ4xJW6zeNnIEOtPTFftJNNIxQnt4JDh2v8fd5hbg0MkDHjt-nflVvf53KfPnLR_gSqvyBZzmKXWAz8zbH_ISWLY7cywvmzA5qYtdr-UDetU7-KEwiSbVZH-FY4GYBlcDR4C35nC9CO1z_e8x-0p4WBjvDNYO7Q3UWt7U8EsnAXHGIKcNWO38zFJo0VHqgUCW7zBtwiPCAA5o"

var localToken =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFhNDc0NzAwMzQ0YzdjZTdjY2VhYmIyZGIwODIxMjBlMDJmZDY5MDFkMDIyMDZhNGFmZDdlYzY1MzE1Nzg2MThhZDBiMmFkMjYwYjg4NmVkIn0.eyJhdWQiOiIxIiwianRpIjoiMWE0NzQ3MDAzNDRjN2NlN2NjZWFiYjJkYjA4MjEyMGUwMmZkNjkwMWQwMjIwNmE0YWZkN2VjNjUzMTU3ODYxOGFkMGIyYWQyNjBiODg2ZWQiLCJpYXQiOjE1MTg3NzQ5NjksIm5iZiI6MTUxODc3NDk2OSwiZXhwIjoxNTUwMzEwOTY5LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.CzfK5TLcJ9ay81DvF8Ql7fnsibzbplvLRBoRgJLpmI_NrE0WWHUM2A-eQVic5pHnB1V1qGZ_h0ZZTE_oXq7yYunfVwT5SXa5eHAUQdYPb35UKhrpfSwqeAU1hjQ2IEaQOXvkDfL29_NgpZ_ixErasJcASwxMaLfqbwSldwWa2gF-luxjFX0ZiZF8iXloL6b39Gcn5sWcTmW_4_0j6opKJZeXLn29F2suq6tTXQep6ySUzyOOA-YmiYXoOhJ8qRocVm-4c1HhAj9T7ySYj-737O1pPEJqZAqV1Bx8D50UiyQveJu_GUgvPBVwie7h0zPTOUdaLN_xhkqqGpof0vaoOrt_aF9qDVyFeqIoHyPV09_CH9w6EivHbDHOwhCvw9-HKajBU5gvSskFXPFHaUXJyJblertkeBAqgjZ2o10t2sMcUxK54UDv3BoAQQ5wNVRo50X-rtdGeXlKYGbboFoRVSkuuVZkf9ch2dLLN_QWwxtDjVygVobKhYEy2Ps7YwW-EWprQAsnRppOsp9nS3R8UrgvBepY6DD9LYrIzDcNRBmLjhVxMkPYs8MPivNuez1-MVNJnFOdlX-mQ1qmpyBlFtrAdkOdmA0KUAdee9nJsEyRo8aHSOVBuGMqd9J5c3I2Up3jVMQ9g8FMdcOSJkliIpaMpcJPC_GrBXZkTsHkVAc"

// var token2 = 'SDMFmc5rIfmm4DYkQvv7jVkn7VqQoVRldlE970Cq'
Vue.prototype.$snipeit = axios.create({
  // baseURL: "http://ramnode1.ashdevtools.com:8080/api/v1/",
  baseURL: "http://168.235.96.46:8080/api/v1/",
  // baseURL: "http://192.168.1.171:8080/api/v1/",
  timeout: 5000,
  headers: {
    Authorization:
      "Bearer " +
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjY4Zjg1MzAzNDlhMDhlNzAwODYyYzcwNDM5OTRjMDhhNTBlMWI2NGE0ZWYxNjA0NjM3ZjcwNjc4Y2QyYjViMmQyM2NmNTU0NzExYmMwNTg5In0.eyJhdWQiOiIxIiwianRpIjoiNjhmODUzMDM0OWEwOGU3MDA4NjJjNzA0Mzk5NGMwOGE1MGUxYjY0YTRlZjE2MDQ2MzdmNzA2NzhjZDJiNWIyZDIzY2Y1NTQ3MTFiYzA1ODkiLCJpYXQiOjE1MTQ1MzE4OTYsIm5iZiI6MTUxNDUzMTg5NiwiZXhwIjoxNTQ2MDY3ODk2LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.dyzpV0D-KkEcmPwTXXnCYkSbvKlqCBYDZMBgzNQvRz9ydgygOVEX8RcRigEjtAIYD4vquok1aq3SmHm_4ZNuOyux82sZt6HiucBJCWzZhWJ1t5sEeMuQaWB2hDxQEu8HbS-xRYl4df94BP_vJM-v-FfKCJ3x-QZaY6Srmu9m8hzZWGfG-U7jbBDxDV1RFMyD6506i0B1Z-cTrBBSY4q0V4Ke6JF7LYzX_sm3KoV6NOEjGqKtNqWwOOfCXfdwgsBV3GUCZCRDBdBXQSACBN8cUl5syalP9tIIuZ4Zzzcxff-4WzuEWwkQL5gYSX5bESG59NmEDFyGI7dmZXt9ytHOsQZGy0bOgRJkNtqAID1F-0TXduMr0pJzszRtVawqqrVWQ2oS2UvI8JKzv4F5c7m9A6l3G9i3CnWT5wgN8QLNsh0D7PlrmRQQl5P8D4WjxfHsvyKn7cp4oH605Rupbx6UG-TsFR4RoNQQ4xJW6zeNnIEOtPTFftJNNIxQnt4JDh2v8fd5hbg0MkDHjt-nflVvf53KfPnLR_gSqvyBZzmKXWAz8zbH_ISWLY7cywvmzA5qYtdr-UDetU7-KEwiSbVZH-FY4GYBlcDR4C35nC9CO1z_e8x-0p4WBjvDNYO7Q3UWt7U8EsnAXHGIKcNWO38zFJo0VHqgUCW7zBtwiPCAA5o",
    // "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjMwMWU3ZDZjZWNkZTI0NzM2ODkxNDVmYTQxNDJjYjQ5Zjc5MWVkMzVkMjAzNTBjZjE1NzE3Y2E0NTYzNmE2ZTY4NmZhODAwZDRmMjkzMjYwIn0.eyJhdWQiOiIxIiwianRpIjoiMzAxZTdkNmNlY2RlMjQ3MzY4OTE0NWZhNDE0MmNiNDlmNzkxZWQzNWQyMDM1MGNmMTU3MTdjYTQ1NjM2YTZlNjg2ZmE4MDBkNGYyOTMyNjAiLCJpYXQiOjE1MTg3NzU2MjEsIm5iZiI6MTUxODc3NTYyMSwiZXhwIjoxNTUwMzExNjIxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.BUqjT2ke4hi8Sr2trXLaD67FRiKuerGGdX8bKRB3YngdpzuKbt_m4ZnfbcGHwfV4xgxNVxl_fCYzfFvjFW2-izKa8LCWxapvHhFkwbBGUhDhPcMifXX-fOYwIELCVYjuUe26yVCMKP0jXENdj9z2Klc8wA0kTVKAjEXowwj6Kbbwxvn23LhMqjuTdl2PGeYu0dTRpfBH4ukp-HsMhqS3LqcU7gZ0oinJd2K_DwLxlJXCjfVJZhHfuyI-AaCjYmVsAqByrqG36zt_Gj5Df7TThzQcd5f3Bt0VrdAe-9ri94IN3WTkZirEgpPtHbNStlEXSm0MIy47pq0g1_FbyfqsnrXmIjE_tV_YVGuEwYUyZAUIjSLgX2qN7mas3hsuyn31o07HNLGzsxQvwMvep6d100yWMPrITJVeala26P1bg6h5HHR33b4wlM9kvxsMdskQkChryAl7b-gy5AvBDLeO5-t7wMKTXRDAiflVNHI77sDU-smtUpXMjikiFl0nZA-gIIaELv1_JhpImNx6i5LFTq2xSEamqwMLntBagDA5wyMZtxYPlO16iEbVhsmOxNZNsqZaDwFBdsCt4XgUeQ95BiTnNwsZZ6ttnV6y33LWljr3bLZ-SggNeori3JMwKaga-qH5qesjzsOrAE02rQfyzCJWGlN5fyVu5qLFlDx3HA4",
    Accept: "application/json",
  },
})

Vue.prototype.$firebase = firebase.initializeApp(config)

Vue.router = router

// Vue.use(VeeValidate, veeValidateConfig)
Vue.use(VueAxios, axios)

Vue.use(NProgress)

// Enable devtools
Vue.config.devtools = true

sync(store, router)

const nprogress = new NProgress({ parent: ".nprogress-container" })

const { state } = store

router.beforeEach((route, redirect, next) => {
  if (state.app.device.isMobile && state.app.sidebar.opened) {
    store.commit(TOGGLE_SIDEBAR, false)
  }
  // Check route meta to see if it requires basic auth
  // if (route.matched.some(record => record.meta.requiresAuth)) {
  if (route.path != "/login-firebase") {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state.auth.loggedIn) {
      next({
        path: "/login-firebase",
        query: { redirect: route.fullPath },
      })
      store.dispatch("setNotification", {
        message: "You are not logged in.",
        type: "danger",
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

const app = new Vue({
  router,
  store,
  nprogress,
  ...App,
})

export { app, router, store }
