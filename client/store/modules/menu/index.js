import * as types from '../../mutation-types'
import lazyLoading from './lazyLoading'
import uifeatures from './uifeatures'
import components from './components'
import tables from './tables'
import assets from './assets'

// show: meta.label -> name
// name: component name
// meta.label: display label

const state = {
  items: [
    // {
    //   name: 'Dashboard',
    //   path: '/dashboard',
    //   meta: {
    //     icon: 'fa-tachometer',
    //     link: 'dashboard/index.vue'
    //   },
    //   component: lazyLoading('dashboard', true)
    // },
    {
      name: 'Live View',
      path: '/liveview',
      meta: {
        icon: 'fa-tachometer',
        link: 'liveview/index.vue'
      },
      component: lazyLoading('liveview', true)
    },
    {
      name: 'Your Dashboard',
      path: '/dashboard',
      meta: {
        icon: 'fa-tachometer',
        link: 'dashboard/index.vue'
      },
      component: lazyLoading('dashboard', true)
    },
    {
      name: 'Hardware',
      path: '/assets/hardware',
      meta: {
        icon: 'fa-tachometer',
        link: 'assets/hardware/index.vue'
      },
      component: lazyLoading('assets/hardware', true)
    },
    {
      name: 'Consumables',
      path: '/consumables',
      meta: {
        icon: 'fa-tachometer',
        link: 'consumables/index.vue'
      },
      component: lazyLoading('assets/consumables', true)
    },
    {
      name: 'Groups',
      path: '/groups',
      meta: {
        icon: 'fa-tachometer',
        link: 'consumables/index.vue'
      },
      component: lazyLoading('assets/consumables', true)
    },
    // assets,
    // uifeatures,
    components
    // tables
  ]
}

const mutations = {
  [types.EXPAND_MENU] (state, menuItem) {
    if (menuItem.index > -1) {
      if (state.items[menuItem.index] && state.items[menuItem.index].meta) {
        state.items[menuItem.index].meta.expanded = menuItem.expanded
      }
    } else if (menuItem.item && 'expanded' in menuItem.item.meta) {
      menuItem.item.meta.expanded = menuItem.expanded
    }
  }
}

export default {
  state,
  mutations
}
