import lazyLoading from './lazyLoading'

export default {
  name: 'Assets',
  path: '/assets',
  meta: {
    icon: 'fa-table',
    expanded: false,
    label: 'Assets'
  },
  component: lazyLoading('assets', true),

  children: [
    {
      name: 'Assets',
      path: '',
      component: lazyLoading('assets/Hardware'),
      meta: {
        label: 'Hardware',
        link: 'assets/Hardware.vue',
        api: 'https://168.235.96.46:8080'
      }
    },
    {
      name: 'Hardware',
      path: 'hardware',
      meta: {
        label: 'Hardware',
        link: 'assets/Hardware.vue',
        api: 'https://168.235.96.46:8080'
      },
      component: lazyLoading('assets/Hardware')
    },

    {
      name: 'Consumables',
      path: 'assets/consumables',
      meta: {
        label: 'Consumables',
        link: 'assets/Consumables.vue',
        api: 'https://168.235.96.46:8080'
      },
      component: lazyLoading('assets/Consumables')
    },
    {
      name: 'Groups',
      path: 'groups',
      meta: {
        label: 'Groups',
        link: 'assets/Groups.vue',
        api: 'https://168.235.96.46:8080'
      },
      component: lazyLoading('assets/Groups')
    }
  ]
}
